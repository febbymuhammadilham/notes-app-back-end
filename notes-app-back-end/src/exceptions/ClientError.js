

class ClientError extends Error {
    constructor(message, statusCode = 400) {
        //messege disini adalah bawaan dari class Error, sehingga bisa lebih detail ,

        //Pesan yang dihasilkan server error bukan ditujukan untuk client, melainkan untuk kita sebagai developer. Tujuannya, agar kita dapat mengetahui penyebab terjadinya server error dan penanganannya menjadi lebih cepat. 
        
      super(message);
      this.statusCode = statusCode;
      this.name = 'ClientError';
    }
  }
   
  module.exports = ClientError;